import time
import random as rand

start1 = time.time()
def fibo_ite(n):
    un0 = 1
    un1 = 1
    un2 = 0
    for i in range (n):
        un2=un1+un0
        un0 = un1
        un1 = un2
        i += 1
fibo_ite(10000)
end1 = time.time()
time1 = end1-start1
print('Temps fibo itératif : ', time1)

start2 = time.time()
def fibo_rec(n):
    if n>1:
        return fibo_rec(n-1) + fibo_rec(n-2)
    elif n==1:
        return 1
    elif n==0:
        return 1
fibo_rec(5)
end2 = time.time()
time2 = end2-start2
print('Temps fibo récursif : ', time2)

start3 = time.time()
def fibo_log(n):
    even = lambda n: (n % 2 == 0)
    (current, next, p, q) = (0, 1, 0, 1)    

    while (n > 0):
        if (even(n)):
            (p, q) = (p**2 + q**2, q**2 + 2*p*q)
            n /= 2
        else:
            (current, next) = (p*current + q*next, q*current + (p+q)*next)
            n -= 1
    return current
fibo_log(1000)
end3 = time.time()
time3 = end3-start3
print('Temps fibo logarithmique : ', time3)

start4 = time.time()
def crible(n):
    liste=[]

    for i in range(2,n):
        liste.append(i)

    for i in liste:
        for n in liste:
            if n % i == 0 and i != n:
                liste.remove(n)
    return liste
crible(1000)
end4 = time.time()
time4 = end4-start4
print('Temps crible : ', time4)



def couples(Pn):
    list = []
    for i in range(len(Pn)):
        for j in range(i+1, len(Pn)):
            temp = [Pn[i], Pn[j]]
            list.append(temp)
    return list

def ensembles(Pn, ind):

    list = couples(Pn)
    temp0 = list[ind][0]
    temp1 = list[ind][1]

    Pn.remove(temp0)
    Pn.remove(temp1)

    if temp1%temp0 == 0 :
        return Pn + [temp1 + temp0], Pn + [temp1 - temp0], Pn + [temp1 * temp0], Pn + [temp1 / temp0]
    else :
        return Pn + [temp1 + temp0], Pn + [temp1 - temp0], Pn + [temp1 * temp0]

def isReachable(Pn, R):
    if len(Pn) == 1:
        if Pn[0] == R: 
            return True
        else:
            return False
    couples = couples(Pn)
    for i in range(len(couples)):
        next = ensembles(Pn, i)
        for j in next:
            if isReachable(Pn, i):
                print(j, next[j])
                return True

def random():
    tirage = []

    list = [1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 25, 25, 50, 50, 75, 75, 100, 100]

    for i in range(5):
        x = rand.randint(0, 28-i)
        tirage.append(list[x])
        del list[x]
    return tirage